import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentSecurePage } from './payment-secure';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PaymentSecurePage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentSecurePage),
    TranslateModule.forChild()
  ],
})
export class PaymentSecurePageModule {}
